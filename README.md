# First Person interactive experience

![](https://gitlab.com/pm_ranj/ucalgary-asgmnt1/-/raw/master/Screenshots/vlcsnap-2021-12-18-10h33m46s487.png)

### Description
An interactive experience showcasing the FPS game interaction systems.
   > ## Including systems
   >1. Generic interaction system
   >2. Non-Playable-Character (NPC) Behavior system
   >3. FPS locomotion system
   >4. NPC Navigation system
   >5. Dynamic IK for NPC look

### Cloning the project
   >1. Clone the source code
   >2. Open the project with **2020.3.22f1** Unity editor version
   
