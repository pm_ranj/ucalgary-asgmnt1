﻿using System;
using DG.Tweening;
using FPSController;
using LightHouse.Extensions;
using UnityEngine;

namespace NpcUtils
{
    public class NpcMotor : MonoBehaviour, IMovement
    {
        [SerializeField] private CharacterController _characterController;
        [SerializeField] private Path _path;
        [SerializeField] private float _speed;
        [SerializeField] private float _lookRadius = 1f;
        [SerializeField] private NpcAnimations _animations;
        [SerializeField] private bool _startOnAwake;
        private Vector3 _destination;
        private Vector3 _direction;

        private bool EndOfPath => _path.EndOfPath;

        private bool _isMoving;
        
        private void Start()
        {
            if(_startOnAwake) StartNavigation();
        }

        public void StartNavigation()
        {
            SetupDestination();
            _isMoving = true;
        }

        public void Wait(float time)
        {
            Stop();
            DOVirtual.DelayedCall(time, () => _isMoving = true);
        }
        public void Stop()
        {
            _isMoving = false;
        }

        public void Continue()
        {
            _isMoving = true;
        }

        public void RestNavigation()
        {
            _path.RestIndex();
        }
        private void SetupDestination()
        {
            _destination = _path.GetNext(transform.position.y);
        }

        private void Update()
        {
            if (!_isMoving) return;
            if (EndOfPath) return;
            
            ChooseTarget();
            ApplyMotion();
            ApplyGravity();
            ApplyRotation();
            Move();

        }

        private void Move()
        {
            _animations.PlayWalk();
            _characterController.Move(MotionDirection);
        }

        private void ChooseTarget()
        {
            if (_path.GetDistance(transform.position) > _lookRadius) return;
            _path.OnReachTheNode();
            if (_path.EndOfPath)
            {
                return;
            }
            SetupDestination();
        }

        public void ApplyRotation()
        {
            var rotation = Quaternion.LookRotation(_direction);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime);
        }

        public void ApplyMotion()
        {
            var dir = _destination - transform.position;
            _direction = dir.normalized;
            
            MotionDirection = _direction * _speed * Time.deltaTime;
        }

        public void ApplyGravity()
        {
            MotionDirection = MotionDirection.GetWithY(-.5f);
        }
        
        

        public Vector3 MotionDirection { get; private set; }
        public bool OnGround => _characterController.isGrounded;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(transform.position, _destination);

        }
    }
}