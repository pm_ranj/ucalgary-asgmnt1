﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using LightHouse.Extensions;
using UnityEditor;
using UnityEngine;

namespace NpcUtils
{
    public class Path : MonoBehaviour 
    {

        [SerializeField] private Color _lineColor;
        [SerializeField] private bool _circuit;
        [SerializeField] private bool _drawInScene;
        [SerializeField] private bool _useWaypoint;
        [SerializeField]  Waypoint[] _waypoints;

        private List<Transform> _nodes = new List<Transform>();
        private int _index;

        public bool EndOfPath => _index >= _nodes.Count;

        public Vector3 CurrentDestination { get; private set; }


        public Transform this[int index] => _nodes[index];

        private string _baseNameOfNodes;
        
        public bool UseWaypoint => _useWaypoint;

        public int NodeCount => _nodes.Count;

        private void Awake()
        {
            BuildNodeList();
            BuildWaypoints();

        }

        private void Start()
        {
            _index = 0;
        }

        public Vector3 GetPosition(int index)
        {
            if (_useWaypoint)
            {
                return _waypoints[index].Position;
            }

            return _nodes[index].position;
        }

        public Vector3 GetNext()
        {
            if(EndOfPath) throw new Exception("end of path");
            
            var pos = GetPosition(_index);
            CurrentDestination = pos;
            Increment();
            return pos;
        }
        
        
        public Vector3 GetNext(float y)
        {
            return GetNext().GetWithY(y);
        }

        public void OnReachTheNode()
        {
            if(!_useWaypoint) return;
            var i = _index - 1 >= 0 ? _index - 1 : 0;
            if(i >= _waypoints.Length) return;
            _waypoints[i].Invoke();
        }

        public float GetDistance(Vector3 navigator)
        {
            return Vector3.Distance(CurrentDestination.GetWithY(navigator), navigator);
        }

        private void Increment()
        {
            _index++;
            if (!EndOfPath) return;
            if (_circuit)
            {
                _index = 0;
            }
        }


        private void RenameNodes()
        {
            for (int i = 0; i < _nodes.Count; i++)
            {
                var number = i;
                if (_nodes[i] != null)
                    _nodes[i].gameObject.name = _baseNameOfNodes + " " + number;
            }
            
            BuildWaypoints();

        }


        private void BuildWaypoints()
        {
            if (!_useWaypoint) return;
            _waypoints = new Waypoint[_nodes.Count];
            for (int i = 0; i < _nodes.Count; i++)
            {
                _waypoints[i] = _nodes[i].GetComponent<Waypoint>();
            }
        }

        private void BuildNodeList()
        {
            Transform[] pathTransforms = GetComponentsInChildren<Transform>();
            _nodes = new List<Transform>();

            for(int i = 0; i < pathTransforms.Length; i++) {
                if(pathTransforms[i] != transform) {
                    _nodes.Add(pathTransforms[i]);
                }
            }
        }
#if UNITY_EDITOR

        private void OnDrawGizmosSelected()
        {
            RenameNodes();
        }

        void OnDrawGizmos() {
            if(!_drawInScene) return;


        Gizmos.color = _lineColor;

            Transform[] pathTransforms = GetComponentsInChildren<Transform>();
            _nodes = new List<Transform>();

            for(int i = 0; i < pathTransforms.Length; i++) {
                if(pathTransforms[i] != transform) {
                    _nodes.Add(pathTransforms[i]);
                }
            }

            for(int i = 0; i < _nodes.Count; i++) {
                Vector3 currentNode = _nodes[i].position;
                Vector3 previousNode = Vector3.zero;
                Gizmos.DrawSphere(currentNode, 0.1f);
                    DrawWaypointWings(currentNode, i);
                if (i > 0) {
                    previousNode = _nodes[i - 1].position; 
                    Gizmos.DrawLine(previousNode, currentNode);

                    Handles.Label(new Vector3(currentNode.x  ,currentNode.y , currentNode.z+ 0.7f), _nodes[i].name );


                } else if(i == 0 && _nodes.Count > 1) {
                    if (_circuit)
                    {
                        previousNode = _nodes[_nodes.Count - 1].position;
                        Gizmos.DrawLine(previousNode, _nodes[0].position); 
                    }


                }

            }
        }

        private void DrawWaypointWings(Vector3 currentNode, int i)
        {
            if (_useWaypoint)
            {
                if (_waypoints == null)
                {
                    BuildWaypoints();
                }

                if (_waypoints[i].XAxis)
                {
                    var right = new Vector3(currentNode.x + _waypoints[i].RightOffset, currentNode.y, currentNode.z);
                    Gizmos.DrawLine(currentNode, right);
                    var left = new Vector3(currentNode.x - _waypoints[i].LeftOffset, currentNode.y, currentNode.z);
                    Gizmos.DrawLine(currentNode, left);
                }
                else
                {
                    var right = new Vector3(currentNode.x, currentNode.y, currentNode.z + _waypoints[i].RightOffset);
                    Gizmos.DrawLine(currentNode, right);
                    var left = new Vector3(currentNode.x, currentNode.y, currentNode.z - _waypoints[i].LeftOffset);
                    Gizmos.DrawLine(currentNode, left);
                }

            }
        }

        private void OnValidate()
        {
            BuildWaypoints();
        }
        
#endif

        public void RestIndex()
        {
            _index = 0;
        }
    }
}
