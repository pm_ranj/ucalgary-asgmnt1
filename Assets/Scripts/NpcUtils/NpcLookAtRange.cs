﻿using System;
using Extensions;
using LightHouse.Extensions;
using UnityEngine;

namespace NpcUtils
{
    public class NpcLookAtRange : MonoBehaviour
    {
        [SerializeField] private NpcLook _look;
        [SerializeField] private NpcDialoguePlayer _voicePlayer;
        [SerializeField] private float _range;
        [SerializeField] private Vector3 _centerOffset;
        [SerializeField] private NpcDialogueQueue _voices;

        [SerializeField] private LayerMask _lookLayer;
        private Collider[] _collidersAllocation;

        private bool _talk;
        
        
        private void Awake()
        {
            _talk = true;
            _collidersAllocation = new Collider[2];

        }

        private void OnEnable()
        {
            _look.onLook += PlayVoice;
        }

        private void OnDisable()
        {
            _look.onLook -= PlayVoice;
        }

        private void Update()
        {
            if (PlayerInRange())
            {
                Look();
            }
            else
            {
                LookBack();
            }
        }

        public void LookAtWall() => _look.LookAtWall();

        public void Look()
        {
            if(!_talk) return;
            _look.LookAtPlayer();
        }

        public void LookBack()
        {
            if(!_talk) return;
            _look.LookBack();
        }

        public void HardLook()
        {
            _look.HardLook();
        }

        public void Mute() => _talk = false;
        public void UnMute() => _talk = true;
        private void PlayVoice()
        {
            if(!_talk) return;
            if(WeightedRandom.Luck(0.7f))
                _voicePlayer.Play(_voices.GetRandom());
        }
        private bool PlayerInRange()
        {
            var overlaps = Physics.OverlapSphereNonAlloc(transform.position + _centerOffset, _range, _collidersAllocation, _lookLayer);
            return overlaps > 0;
        }
        
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;

            var transform1 = transform;
            var origin = transform1.position;
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(origin + _centerOffset, _range);
        }

#endif
    }
}