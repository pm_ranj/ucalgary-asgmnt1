﻿using System;
using UnityEngine;

namespace NpcUtils
{
    [Serializable]
    public class NpcDialogue
    {
        [SerializeField] private string text;
        [SerializeField] private AudioClip voice;

        public string Text => text;

        public AudioClip Voice => voice;
    }
}