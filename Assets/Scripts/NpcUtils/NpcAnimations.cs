﻿using UnityEngine;

namespace NpcUtils
{
    public class NpcAnimations : MonoBehaviour
    {
        [SerializeField] private Animator _animator;

        public void PlayWalk()
        {
            Play("Walk");
        }

        public void PlayIdle()
        {
            Play("Idle");
        }


        public void PlayInteract()
        {
            Play("Interact");
        }
        
        public void PlayTalk()
        {
           Play("Talk");
        }        
        public void PlaySit()
        {
           Play("Sit");
        }
        
        private void Play(string trigger, bool overdue = true)
        {
            if (overdue && !IsPlaying(trigger))
                _animator.SetTrigger(trigger);
        }

        private bool IsPlaying(string animTag)
        {
            return _animator.GetCurrentAnimatorStateInfo(0).IsTag(animTag);
        }

    }
}