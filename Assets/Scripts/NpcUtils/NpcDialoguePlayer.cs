﻿using System;
using UnityEngine;

namespace NpcUtils
{
    public class NpcDialoguePlayer : MonoBehaviour
    {
        [SerializeField] private AudioSource _source;

        public event Action<string> onPlay;


        private void Awake()
        {
            _source.loop = false;
        }

        public void Play(NpcDialogue dialogue)
        {
            _source.clip = dialogue.Voice;
            _source.Play();
            onPlay?.Invoke(dialogue.Text);
        }
    }
}