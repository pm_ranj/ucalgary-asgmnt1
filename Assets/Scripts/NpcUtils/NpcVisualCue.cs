﻿using UnityEngine;
using UnityEngine.UI;

namespace NpcUtils
{
    public class NpcVisualCue : MonoBehaviour
    {
        [SerializeField] private Sprite _talkingSprite;
        [SerializeField] private Sprite _interactionAvailableSprite;
        [SerializeField] private Sprite _interactionFinished;
        
        [SerializeField] private Image _iconHolder;


        public void Talking() => _iconHolder.sprite = _talkingSprite;
        public void InteractionAvailable() => _iconHolder.sprite = _interactionAvailableSprite;
        public void InteractionEnded() => _iconHolder.sprite = _interactionFinished;




    }
}