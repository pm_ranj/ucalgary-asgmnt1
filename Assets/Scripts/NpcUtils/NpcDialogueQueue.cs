﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace NpcUtils
{
    [Serializable]
    public class NpcDialogueQueue
    {
        [SerializeField] private NpcDialogue _default;
        [SerializeField] private NpcDialogue[] _dialogues;

        public bool EndOfQueue => _index >= _dialogues.Length;
        private int _index;

        public NpcDialogue GetNext()
        {
            if (_index >= _dialogues.Length) return _default;

            var dialogue = _dialogues[_index];
            _index += 1;
            return dialogue;
        }

        public NpcDialogue GetRandom()
        {
            var randomIndex = Random.Range(0, _dialogues.Length);
            return _dialogues[randomIndex];
        }
        
        public void Reset()
        {
            _index = 0;
        }
    }
}