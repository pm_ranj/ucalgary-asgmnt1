﻿using LightHouse.Extensions;
using UnityEngine;

namespace NpcUtils
{
    public class NpcFootsteps : MonoBehaviour
    {
        [SerializeField] private AudioSource _source;
        [SerializeField] private AudioClip [] _sounds;
        [SerializeField] private Vector2 _pitchOffset;


        private AudioClip GetRandom() => _sounds[Random.Range(0, _sounds.Length)];

        public void PlayFootsteps()
        {
            _source.pitch += _pitchOffset.GetRandomBetween();
            _source.PlayOneShot(GetRandom());
        }

    }
}