using UnityEngine;
using UnityEngine.Events;

namespace NpcUtils
{
    public class Waypoint : MonoBehaviour
    {
        [SerializeField] [Range(0f, 2f)] private float rightOffset;
        [SerializeField] [Range(0f, 2f)] private float leftOffset;

        [SerializeField] private UnityEvent onReachNode;
        
        [SerializeField]  [HideInInspector]private bool xAxis = true;
        public float RightOffset => rightOffset;

        public float LeftOffset => leftOffset;
        
        public bool XAxis => xAxis;

        public Vector3 Position => GetPosition();
        

        public void Invoke()
        {
            onReachNode?.Invoke();
        }
        private Vector3 GetPosition()
        {
            var pointTransform = transform;

            if (xAxis)
            {
                var x = transform.position.x + Random.Range(-leftOffset, rightOffset);
                return new Vector3(x, pointTransform.localPosition.y, pointTransform.position.z);
            }

            var z = transform.position.z + Random.Range(-leftOffset, rightOffset);
            return new Vector3(pointTransform.position.x, pointTransform.localPosition.y, z);
        }
    }
}