﻿using System;
using UnityEngine;

public class InputProvider : MonoBehaviour
{
    public bool WASDDown => !_muteKeyboard && WASDInput.magnitude > 0f;

    public bool MouseInputDown => !_muteMouse;
    public Vector2 MouseInput { get; private set; }
    public Vector2 WASDInput { get; private set; }

    [SerializeField] private bool _muteKeyboard;
    [SerializeField] private bool _muteMouse;
    private void Update()
    {
        ReadMouse();
        ReadWASD();
    }

    public void SetMouseMute(bool mute)
    {
        _muteMouse = mute;
    }
    public void SetKeyboardMute(bool mute)
    {
        _muteKeyboard = mute;
    }
       
    public void SetInputMute(bool mute)
    {
        _muteKeyboard = mute;
        _muteMouse = mute;

    }
    

    private void ReadMouse()
    {
        if(_muteMouse) return;
            
        var mouseX = Input.GetAxis("Mouse X");
        var mouseY = Input.GetAxis("Mouse Y");
        MouseInput = new Vector2(mouseX, mouseY);
    }

    private void ReadWASD()
    {
        if (_muteKeyboard) return;   
        var horizontalKeys = Input.GetAxis("Horizontal");
        var verticalKeys = Input.GetAxis("Vertical");

        WASDInput = new Vector2(horizontalKeys, verticalKeys);
    }
}