﻿using System;
using UnityEngine;

namespace DefaultNamespace
{
    public class AlwaysLookAtPlayer : MonoBehaviour
    {
        [SerializeField] private Vector3 _lookOffset;
        [SerializeField] private Transform _player;

        [SerializeField] private float _detectionRadius = 5f;
        private Collider[] _detectionBuffer;

        private LayerMask _playerLayer;
        
        private void Awake()
        {
            _detectionBuffer = new Collider[1];
            _playerLayer = LayerMask.NameToLayer("Player");
        }


        private void Update()
        {
            if(!CheckPlayerAround())  return;
            LookAt();
        }

        private bool CheckPlayerAround()
        {
            var detections = Physics.OverlapSphereNonAlloc(transform.position, _detectionRadius, _detectionBuffer,
                _playerLayer);
            return detections > 0;
        }
        private void LookAt()
        {
            transform.LookAt(_player.position + _lookOffset);
        }
    }
}