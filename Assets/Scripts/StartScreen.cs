﻿using System;
using Cinemachine;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    public Text[] texts;
    private bool _started;
    public InputProvider input;
    public CinemachineVirtualCamera startCam;

    private void Awake()
    {
        input.SetInputMute(true);
        startCam.Priority = 100;
    }

    public void Update()
    {
        if (!_started)
        {
            if (!Input.anyKey) return;
            _started = true;
            Disappear();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.F2))
            {
                SceneManager.LoadScene("SampleScene");
            }
        }
    }

    private void Disappear()
    {
        var sec = DOTween.Sequence();
        foreach (var text in texts)
        {
            sec.Join(text.DOFade(0f, 1.5f));
        }

        sec.onComplete += () => input.SetInputMute(false);

        sec.Play();
        startCam.Priority = 0;
    }
}