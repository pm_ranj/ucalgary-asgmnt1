﻿using System;
using System.Collections;
using NpcUtils;
using UnityEngine;

namespace Interactions
{
    public class InteractableNpc: InteractalbeObject<InteractableNpcProperties>
    {
        [SerializeField] private NpcInteractionQueue _interactionQueue;
        [SerializeField] private NpcVisualCue _visualCue;
        [SerializeField] private NpcDialoguePlayer _dialoguePlayer;

        [SerializeField] private NpcLookAtRange _look;
        private bool _isInteracting;
        private void Start()
        {
            if(!_interactionQueue.EndOfQueue)
                _visualCue.InteractionAvailable();

        }

        protected override void OnDetected()
        {
        }

        protected override void OnUnDetected()
        {
        }

        protected override void OnInteract()
        {
            if (_isInteracting) return;
            if (_interactionQueue.EndOfQueue)
            {
                _visualCue.InteractionEnded();
                return;
            }        
            _look.LookBack();
            _look.Mute();
            _visualCue.Talking();
            Do(_interactionQueue.GetNext());
        }

        protected override void OnInteractionFinished()
        {
            _look.UnMute();
            if (_interactionQueue.EndOfQueue)
            {
                _visualCue.InteractionEnded();
            }
            else
            {
                _visualCue.InteractionAvailable();
            }
            
        }

        private void Do(NpcInteraction interaction) => StartCoroutine(StartInteraction(interaction));

        private IEnumerator StartInteraction(NpcInteraction interaction)
        {
            if(_isInteracting) yield break;
            _isInteracting = true;
            interaction.DoBeforeDialogue();
            _dialoguePlayer.Play(interaction.Dialogue);
            yield return new WaitForSeconds(interaction.Dialogue.Voice.length);
            interaction.DoAfterDialogue();
            yield return new WaitForSeconds(interaction.InteractionDuration);
            _isInteracting = false;
            OnInteractionFinished();
        }
    }
}