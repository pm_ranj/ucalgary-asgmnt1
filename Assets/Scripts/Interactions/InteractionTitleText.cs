﻿using System;
using DG.Tweening;
using LightHouse.Extensions;
using UnityEngine;
using UnityEngine.UI;

namespace Interactions
{
    public class InteractionTitleText : MonoBehaviour
    {
        [SerializeField] private Text _interactionNameHolder;

        public string Text
        {
            get => _interactionNameHolder.text;
            set => _interactionNameHolder.text = $"[E] {value}";
        }


        private void Start()
        {
            _interactionNameHolder.color = _interactionNameHolder.color.GetWhiteAlpha(0f);
        }

        public void Appear()
        {
            _interactionNameHolder.DOFade(1f, 0.1f);
        }

        public void Disappear()
        {
            _interactionNameHolder.DOFade(0f, 0.1f);

        }
        
        
    }
}