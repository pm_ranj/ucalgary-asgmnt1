﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Interactions
{
    public class InteractableGenericHandle : InteractalbeObject<InteractableHandleProperties>
    {
        [SerializeField] private GlowingMaterial _material;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private UnityEvent _onInteract;
        
        protected override void OnDetected()
        {
            _material.GlowObject(properties.FadeInDuration);
        }

        protected override void OnUnDetected()
        {
            _material.DimObject(properties.FadeOutDuration);
        }

        protected override void OnInteract()
        {
            PlayInteractionSfx();
            _onInteract?.Invoke();
            _material.BlinkObject(properties.InteractingColor);
        }

        protected override void OnInteractionFinished()
        {
        }
        private void PlayInteractionSfx()
        {
            if (_audioSource is null) return;
            _audioSource.PlayOneShot(properties.InteractionSfx);
        }
    }
}