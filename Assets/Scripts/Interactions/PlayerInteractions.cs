﻿using System;
using System.Reflection;
using UnityEngine;

namespace Interactions
{
    public class PlayerInteractions : MonoBehaviour
    {
        [SerializeField] private Vector3 _findingCenter;
        [SerializeField] private float _findingRadius;
        [SerializeField] private LayerMask _detectionMask;
        [SerializeField] private KeyCode _interactKey = KeyCode.E;
        private Camera _camera;

        private Collider[] _collidersAllocation;

        private IInteractable _detectedObject;

        private bool _focusing;

        private void Awake()
        {
            _camera = Camera.main;
            _collidersAllocation = new Collider[10];
        }

        private void Update()
        {
            if(!InteractablesAreNear()) return;
            Detect();
            Interact();
        }

        private void Interact()
        {
            if(!Input.GetKeyDown(_interactKey)) return;
            
            if (_detectedObject is null) return;
            
            if(!_focusing) return;
            
            _detectedObject.Interact();
            
            DeactivateSelected();
        }

        private bool InteractablesAreNear()
        {
            var overlaps = Physics.OverlapSphereNonAlloc(_camera.transform.position + _findingCenter, _findingRadius, _collidersAllocation);
            return overlaps > 0;
        }

        private void Detect()
        {
            
            var detected = ShootRay(out var detectedObject);

            if (!detected)
            {
                DeactivateSelected();
                return;
            }

            if(_focusing) return;
            
            SelectInteractableObjectFromRay(detectedObject);
        }

        private void SelectInteractableObjectFromRay(RaycastHit detectedObject)
        {
            var interactable = detectedObject.collider.GetComponent<IInteractable>();
            
            if (interactable is null)
            {
                DeactivateSelected();
                return;
            }

            if (interactable != _detectedObject)
            {
                DeactivateSelected();
            }

            _detectedObject = interactable;

            ActiveInteractale();
        }

        private bool ShootRay(out RaycastHit detectedObject)
        {
            var camTransform = _camera.transform;
            var origin = camTransform.position + _findingCenter;
            var direction = camTransform.forward;
            var ray = new Ray(origin, direction);
            var detected = Physics.Raycast(ray, out detectedObject, _findingRadius, _detectionMask);
            return detected;
        }

        private void ActiveInteractale()
        {
            if(_detectedObject is null) return;
            _focusing = true;
            _detectedObject.Detected();
        }

        private void DeactivateSelected()
        {
            if(_detectedObject is null) return;
            _focusing = false;
            _detectedObject.UnDetected();
        }
        
#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;

            if(_camera == null) _camera = Camera.main;
            
            var camTransform = _camera.transform ;
            var origin = camTransform.position  + _findingCenter;
            var direction = camTransform.forward;
            Gizmos.DrawLine(origin, origin + direction * _findingRadius);
        
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(origin, _findingRadius);
        }

#endif
    }
}