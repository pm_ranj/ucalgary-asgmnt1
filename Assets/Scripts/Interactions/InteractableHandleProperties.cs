﻿using UnityEngine;

namespace Interactions
{
    [CreateAssetMenu(fileName = "Interactable-Handle-Properties", menuName = "Interactables/Handle Properties", order = 0)]

    public class InteractableHandleProperties : InteractableProperties
    {
        [SerializeField] private float _fadeInDuration = 0.1f;
        [SerializeField] private float _fadeOutDuration = 0.1f;
        [SerializeField] private float _blinkDuration = 0.2f;
        [SerializeField] private Color _interactingColor = Color.yellow;
        [SerializeField] private AudioClip _interactionSfx;

        public float FadeInDuration => _fadeInDuration;

        public float FadeOutDuration => _fadeOutDuration;

        public float BlinkDuration => _blinkDuration;

        public Color InteractingColor => _interactingColor;

        public AudioClip InteractionSfx => _interactionSfx;
    }
}