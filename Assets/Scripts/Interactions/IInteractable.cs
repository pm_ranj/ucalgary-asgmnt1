﻿namespace Interactions
{
    internal interface IInteractable
    {
        string InteractionName { get; }
        bool Selected { get; }
        void Detected();
        void Interact();
        void UnDetected();
        

    }
}