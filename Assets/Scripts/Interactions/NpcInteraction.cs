﻿using System;
using NpcUtils;
using UnityEngine;
using UnityEngine.Events;

namespace Interactions
{
    [Serializable]
    public class NpcInteraction
    {
        [SerializeField] private NpcDialogue _dialogue;
        [SerializeField] private float _interactionDuration;
        [SerializeField] private UnityEvent _beforeDialogue;
        [SerializeField] private UnityEvent _afterDialogue;

        public float InteractionDuration => _interactionDuration;

        public NpcDialogue Dialogue => _dialogue;

        public void DoBeforeDialogue() => _beforeDialogue?.Invoke();
        public void DoAfterDialogue() => _afterDialogue?.Invoke();
    }
}