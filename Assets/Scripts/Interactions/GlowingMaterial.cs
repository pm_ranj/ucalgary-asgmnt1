﻿using DG.Tweening;
using UnityEngine;

namespace Interactions
{
    public class GlowingMaterial : MonoBehaviour
    {
        [SerializeField] private float _initialGlow = 0.1f;
        [SerializeField] private MeshRenderer _renderer;
       
        private Color _defaultGlowColor;
        
        private static string MaterialGlowAmountID = "Vector1_cc604b41f9774d5d965822c8c0c09548";
        private static string MaterialGlowColorID = "Color_08f6c14f755540d2a560411803647ffc";
        
        protected void Awake()
        {
            _defaultGlowColor = _renderer.material.GetColor(MaterialGlowColorID);
            _renderer.material.SetFloat(MaterialGlowAmountID, _initialGlow);
        }
        public void GlowObject(float duration) => _renderer.material.DOFloat(1f,MaterialGlowAmountID , duration);
        
        public void DimObject(float duration) =>_renderer.material.DOFloat(_initialGlow, MaterialGlowAmountID, duration);

        public void BlinkObject(Color blinkColor)
        {
            var mat = _renderer.material;
            var seq = DOTween.Sequence();
            seq.Append(mat.DOColor(blinkColor, MaterialGlowColorID, 0.1f));
            seq.AppendInterval(0.05f);
            seq.Append(mat.DOColor(_defaultGlowColor, MaterialGlowColorID, 0.1f));
            seq.Play();
        }
    }
}