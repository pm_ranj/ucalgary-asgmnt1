﻿using System;
using NpcUtils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Interactions
{
    [Serializable]
    public class NpcInteractionQueue
    {
        [SerializeField] private NpcInteraction[] _interactions;
        
        public bool EndOfQueue => _index >= _interactions.Length;
        private int _index;

        public NpcInteraction GetNext()
        {
            if (_index >= _interactions.Length) return null;

            var dialogue = _interactions[_index];
            _index += 1;
            return dialogue;
        }

        public NpcInteraction GetRandom()
        {
            var randomIndex = Random.Range(0, _interactions.Length);
            return _interactions[randomIndex];
        }
        
        public void Reset()
        {
            _index = 0;
        }
    }
}