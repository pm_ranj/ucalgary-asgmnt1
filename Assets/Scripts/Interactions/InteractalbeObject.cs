﻿
using UnityEngine;

namespace Interactions
{
    public abstract class InteractalbeObject<T> : MonoBehaviour, IInteractable where T : InteractableProperties
    {
        [Header("Interaction Setup")]
        [SerializeField] protected T properties;
        [SerializeField] protected string _name;
        [SerializeField] private bool _oneTime;
        
        [SerializeField] private InteractionTitleText _titleText;

        private bool _interacted;
        
        public bool Selected { get; private set; }

        public string InteractionName
        {
            get => _name;
            protected set
            {
                _name = value;
            }
        }

        public bool Muted => _oneTime && _interacted;

        protected void Start()
        {
            _titleText.Text = InteractionName;
        }

        public void Detected()
        {
            if(Selected || Muted) return;
            Selected = true;
            _titleText.Text = InteractionName;
            _titleText.Appear();

            OnDetected();
        }

        public void Interact()
        {
            if (Muted)
            {
                OnInteractionFinished();
                return;
            }
            
            _interacted = true;
            OnInteract();
            UnDetected();
        }

        public void UnDetected()
        {
            Selected = false;
            _titleText.Disappear();

            OnUnDetected();
        }

        protected abstract void OnDetected();
        protected abstract void OnUnDetected();
        protected abstract void OnInteract();
        protected abstract void OnInteractionFinished();


    }
}