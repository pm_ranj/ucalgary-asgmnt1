﻿
using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Animations.Rigging;

[Serializable]
public class NpcLook
{
    [SerializeField] private Rig _headRig;
    [SerializeField] private MultiAimConstraint _constraint;
    [SerializeField] private float _lookTime;
    [SerializeField] private float _lookInterval;
    [SerializeField] private float _lookBackTime;

    private bool _isLooking;

    public event Action onLook;
    public event Action onLookBack;

    public void LookAtPlayer()
    {
        if (_isLooking) return;
        _isLooking = true;
        SetRigWeight(1f, _lookTime);
        onLook?.Invoke();
    }

    public void LookAtWall()
    {
        DOVirtual.Float(_constraint.data.sourceObjects[0].weight, 1f, _lookTime, w =>
        {
            _constraint.data.sourceObjects.SetWeight(0, w);
        });
        DOVirtual.Float(_constraint.data.sourceObjects[1].weight, 0f, _lookTime, w =>
        {
            _constraint.data.sourceObjects.SetWeight(1, w);
        });
    }
    public void LookBack()
    {
        if(!_isLooking) return;
        SetRigWeight(0f, _lookBackTime);

        _isLooking = false;
    }
    private void SetRigWeight(float weight, float time)
    {
        DOVirtual.Float(_headRig.weight, weight, time, w => _headRig.weight = w);
    }

    public void HardLook()
    {
        _isLooking = false;
        LookAtPlayer();
    }
}
