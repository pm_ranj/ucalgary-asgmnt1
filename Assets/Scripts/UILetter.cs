﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class UILetter : MonoBehaviour
    {
        public bool isActive;

        public Text text;
        private void Update()
        {
            if(!isActive) return;
            if(Input.GetKeyDown(KeyCode.Escape)) Disappear();
        }

        public void Toggle()
        {
            if (isActive)
            {
                Disappear();
                return;
            }
            Appear();
        }
        public void Appear() => text.DOFade(1f, 0.8f).onComplete += () => isActive = true;
        public void Disappear() => text.DOFade(0f, 0.4f).onComplete += () => isActive = false;
    }
}