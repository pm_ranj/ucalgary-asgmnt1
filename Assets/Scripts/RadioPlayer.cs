﻿using UnityEngine;

namespace DefaultNamespace
{
    public class RadioPlayer : MonoBehaviour
    {
        [SerializeField] private AudioSource _audioSource;

        public void Toggle()
        {
            if (_audioSource.isPlaying)
            {
                _audioSource.Pause();
            }
            else
            {
                _audioSource.Play();
            }
        }


    }
}