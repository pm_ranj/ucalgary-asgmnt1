﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace DefaultNamespace
{
    public class RoomStructure : MonoBehaviour
    {
        [SerializeField] private Transform _door;
        [SerializeField] private float _openYPos;
        [SerializeField] private float _closeYPos;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private float _actionTime = 0.1f;
        [SerializeField] private AnimationCurve _curve;
        [SerializeField] private UnityEvent _afterMovement;
        [SerializeField] private bool _oneTime;
        private bool _isDoorMoving;

        private bool Muted => _oneTime && _eventTriggered;
        private bool _eventTriggered;
        public void Toggle()
        {
            if(_isDoorMoving) return;
            _isDoorMoving = true;
            _audioSource.Play();

            if (_door.localPosition.y >= _openYPos)
            {
                Close();
                return;
            }

            Open();
        }

        private void Close()
        {
            _door.DOLocalMoveY(_closeYPos, _actionTime).SetEase(_curve).onComplete +=() =>
            {
                if (!Muted)
                {
                    _afterMovement?.Invoke();
                    _eventTriggered = true;
                }
                _audioSource.Stop();
                _isDoorMoving = false;
            };
        }
        private void Open()
        {
            _door.DOLocalMoveY(_openYPos, _actionTime).SetEase(_curve).onComplete +=() =>
            {
                if (!Muted)
                {
                    _afterMovement?.Invoke();
                    _eventTriggered = true;
                }
                _audioSource.Stop();

                _isDoorMoving = false;
            };
        }
    }
}