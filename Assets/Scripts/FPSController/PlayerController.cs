﻿using System;
using UnityEngine;

namespace FPSController
{
    public class PlayerController : MonoBehaviour, IMovement
    {
        [SerializeField] private CharacterController _characterController;

        [SerializeField] private FPSCamera _camera;

        [SerializeField] private InputProvider _input;

        [SerializeField] private float _speed = 100f;
        
        private float _yVelocity = -.5f;
        public bool OnGround => _characterController.isGrounded;
        public Vector3 MotionDirection { get; private set; }


        private void Update()
        {
            ApplyMotion();
            ApplyGravity();
            Move();
        }

        public void Move()
        {
            
            _characterController.Move(MotionDirection);

        }
        public void ApplyRotation()
        {

        }

        public void ApplyMotion()
        {
            if(!_input.WASDDown) MotionDirection = Vector3.zero;
            var xInput = _input.WASDInput.x * _speed * Time.deltaTime;
            var yInput = _input.WASDInput.y * _speed * Time.deltaTime;
            var rightMotion = _camera.Right.normalized * xInput;
            var forwardMotion = _camera.Forward.normalized * yInput;
            MotionDirection = rightMotion + forwardMotion;
        }

        public void ApplyGravity()
        {
            var motionDirection = MotionDirection;
            motionDirection.y = _yVelocity ;
            MotionDirection = motionDirection;
        }
    }
}