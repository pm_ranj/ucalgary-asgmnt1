﻿using UnityEngine;

namespace FPSController
{
    public interface IMovement
    {
        void ApplyRotation();
        void ApplyMotion();
        void ApplyGravity();
        Vector3 MotionDirection { get; }
        bool OnGround { get; }
    }
}