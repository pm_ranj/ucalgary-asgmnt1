﻿using System;
using Cinemachine;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace FPSController
{
    public class FPSCamera : MonoBehaviour
    {
        [SerializeField] private Transform _camTransform;
        [SerializeField] private InputProvider _input;

        [SerializeField] private Vector2 _yRotationLimits = new Vector2(-180f, 180f);

        [SerializeField] private float _sensetivity = 1f;
        private float _mouseX;
        private float _mouseY;

        public Vector3 Forward { get; private set; }
        public Vector3 Right { get; private set; }

        private void Start()
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }

        private void Update()
        {
            ReadInput();
            ApplyRotation();
            UpdateForward();
        }

        private void UpdateForward()
        {
            Forward = _camTransform.forward;
            Right = _camTransform.right;
        }

        private void ApplyRotation()
        {
            _camTransform.transform.localRotation = Quaternion.Euler(_mouseX, _mouseY, 0f);
        }

        private void ReadInput()
        {
            var x= _input.MouseInput.y;
            var y= _input.MouseInput.x;


           _mouseX = Mathf.Lerp(_mouseX, _mouseX - x, _sensetivity * Time.deltaTime);
           _mouseY = Mathf.Lerp(_mouseY, _mouseY + y, _sensetivity * Time.deltaTime);
           _mouseX = Mathf.Clamp(_mouseX, _yRotationLimits.x, _yRotationLimits.y);


        }
    }
}